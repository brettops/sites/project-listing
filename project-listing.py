#!/usr/bin/env python3
import argparse
import logging
import sys
from dataclasses import asdict, dataclass, field
from datetime import datetime

import gitlab
import yaml


@dataclass
class Project:
    description: str
    name: str
    path: str
    full_name: str
    full_path: str
    ref: str
    url: str
    topics: list[str] = field(default_factory=list)


@dataclass
class Section:
    description: str
    name: str
    path: str
    projects: list[Project] = field(default_factory=list)


def put_projects_into_buckets(gl, glgroup, glprojects):
    buckets = {}
    for glproject in glprojects:
        group_id = int(glproject.namespace["id"])
        if group_id not in buckets:
            logging.info(f"retrieving subgroup {group_id}")
            glsubgroup = gl.groups.get(group_id)
            buckets[group_id] = dict(
                section=Section(
                    name=glsubgroup.full_name.removeprefix(f"{glgroup.full_name} / "),
                    path=glsubgroup.full_path.removeprefix(f"{glgroup.full_path}/"),
                    description=glsubgroup.description
                    if glsubgroup.description
                    else "",
                ),
                projects=dict(),
            )
        buckets[group_id]["projects"][int(glproject.id)] = Project(
            description=glproject.description if glproject.description else "",
            name=glproject.name,
            path=glproject.path,
            full_name=glproject.name_with_namespace,
            full_path=glproject.path_with_namespace,
            url=glproject.web_url,
            ref=glproject.default_branch,
            topics=glproject.topics,
        )
    return buckets


def render_bucket_data(buckets):
    data = {}

    for bucket in sorted(buckets.values(), key=lambda x: x["section"].name):
        section_path = bucket["section"].path
        data[section_path] = asdict(bucket["section"])

        for project in sorted(bucket["projects"].values(), key=lambda x: x.name):
            data[section_path]["projects"].append(asdict(project))

    return data


def get_project_listing_from_group(gl, group_id, group_name=None):
    glgroup = gl.groups.get(group_id)
    if group_name:
        if glgroup.full_name != group_name:
            print("Group name does not match expected value", file=sys.stderr)
            sys.exit(1)

    logging.info(f"retrieving project list for group {glgroup.id}")

    glprojects = glgroup.projects.list(
        get_all=True, visibility="public", include_subgroups=True
    )
    # print(yaml.dump(glprojects[0].attributes))

    buckets = put_projects_into_buckets(gl, glgroup, glprojects)
    # print(yaml.dump(buckets))

    data = render_bucket_data(buckets)
    # print(yaml.dump(data))

    return data


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-g", "--group-id", required=True, type=int)
    parser.add_argument("-G", "--group-name", type=str)
    parser.add_argument(
        "-o", "--output", type=argparse.FileType("w"), default=sys.stdout
    )

    args = parser.parse_args()

    logging.basicConfig(level=logging.INFO, format="%(levelname)s %(message)s")

    gl = gitlab.Gitlab()

    data = {}
    data["group_id"] = args.group_id
    data["group_name"] = args.group_name
    data["generated"] = datetime.utcnow().isoformat()
    data["subgroups"] = get_project_listing_from_group(
        gl, args.group_id, group_name=args.group_name
    )

    args.output.write(yaml.dump(data))


if __name__ == "__main__":
    main()
