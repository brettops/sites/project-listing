# project-listing

<!-- BADGIE TIME -->

[![pipeline status](https://gitlab.com/brettops/sites/project-listing/badges/main/pipeline.svg)](https://gitlab.com/brettops/sites/project-listing/-/commits/main)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

<!-- END BADGIE TIME -->

A public listing of all BrettOps projects.
